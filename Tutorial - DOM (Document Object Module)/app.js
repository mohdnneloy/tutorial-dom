/* This is the app.js file.
You need to remember that the file needs to be linked in the body section
of the HTML index file. This file mainly controls the DOM(Document Object Model)
*/

/*As this is a tutorial base coded file most of the codes will be in comments
but you may remove the "//" to activate the code.
*/

/* REMEMBER you while using this js file for the console you need to use the
the console.log(item) code to send the files to the console and there it will
be executed.*/

// #1 Getting the element by ID

//var id1 = document.getElementById('box1');
//console.log(id1);
//var id2 = document.getElementById('box2');
//console.log(id2);

/*The element can be stored as a constant also which will not be able to change
its value through out the program*/

/*const idc1 = document.getElementById('box1');
console.log(idc1);*/

// #2 Getting the element by class // Make sure you use Elements for classes

/*var c1 = document.getElementsByClassName('header');
console.log(c1);*/

// #3 Getting the element by Tag

/*var t1 = document.getElementsByTagName('li');
console.log(t1);*/

/* #4 Talking about arrays. All the HTML collection can be used as array for
console single selecting item. But coming to the point of diplaying each
element of the collection will not work as it is not an array. To check whether
the collection is an array we have the following code. Please make sure you
input the name of the variable in place of the component which you want to
check.*/

//console.log(Array.isArray(t1));

/* #5 Now the next code is to convert a non array variable to an array
 variable*/

//Array.from(t1);
//console.log(Array.isArray(Array.from(t1)));

/* #6 An HTML collection needs to be an Array to display all the selected
 Elements. So as we have already converted it to an array now we will display
 all the elements. There are ways to that. */

// 1. Using array command
/*Array.from(t1).forEach(function(t1){
  console.log(t1);
})*/

// 2. Using the for loop
/*for(i=0;i<t1.length;i++){
  console.log(t1[i]);
}*/

/* #7 Let's move on to the querySelector. Using the querySelector we can select
anything. querySelector is used to select one item. On the other hand
querySelectorAll. We can write the commond like following form also.
Another point to note is that when you select any item by using any of the
querySelector the list will be a Nodelist which will be an Array already.*/


/*console.log(document.querySelector('#box2 li '));
//This Selects oneElement.
console.log(document.querySelectorAll('#box2 li')); //This Selects Multiple.*/

/* #8 Selecting a particular item within the querySelector needs more efficient
coding. Using the pseudo form we can select a particular element but it also
counts the elements after on li that is the <br/> tag also.*/

/*var q1 = document.querySelector('#box2 .BookNames li:nth-child(7)');
console.log(q1);*/

/* #9 Displaying all elements when it is a NodeList */

/*var q1 =document.querySelectorAll('#box2 li');
q1.forEach(function(q1){
  console.log(q1);
})*/

/* #10 Updating the data within the element or displaying that but for that we
will need the multiple element diplay code.*/


// 1. Displaying the text in the tag.
/*var books = document.querySelectorAll('#box2 li .name');
books.forEach(function(books){
  console.log(books.textContent);
});*/

// 2. Updating data by removing the earlier content in it.
/*var books = document.querySelectorAll('#box2 li .name');
books.forEach(function(books){
  books.textContent = " Enter any text";
});*/

// 3. Updating the data by appending or including content to the previous one.
/*var books = document.querySelectorAll('#box2 li .name');
books.forEach(function(books){
  books.textContent += ' Enter any text';
});*/
/* #11 Updating the existing HTML tags when required*/

// 1. Displaying the selected HTML to be updated
/*var bookH = document.querySelector('#box2');
console.log(bookH.innerHTML);*/

// 2. Updating the data within the HTML. This is only to change text
/*var bookH = document.querySelector('#box1 h3'); // Selection is here
bookH.innerHTML = "<h1> Hello </h1>";
// Changing is done here */

 // 3. Updating the data by appending or including content to the previous one.
 /*var bookH = document.querySelector('#box1 h3'); // Selection is here
 bookH.innerHTML += "<h1> Hello </h1>";*/

 /* #12 Talking about the nodes which are also every single element  in the
 DOM*/

 // 1. Finding the type of Node. Each node has a type mnumber.

 /*var banner = document.querySelector('#box1');
 console.log('#box1 is of node type:', banner.nodeType);*/

 // 2. Finding the name of the node.
 /*var banner = document.querySelector('#box1');
 console.log('#box1 is of node name:', banner.nodeName);*/

 // 3. Finding if any of the nodes contain childs or not.
 /*var banner = document.querySelector('#box1');
 console.log('#box1 has child or not:', banner.hasChildNodes());*/

// 4. Cloning the node if true with child if false without child
 /*var banner = document.querySelector('#box1');
 console.log(banner.cloneNode(true));
 console.log(banner.cloneNode(false));*/

/* #13 Traversing through the DOM (Part 1) */

/*var p1 = document.querySelector('#box2');
console.log(p1.parentNode);
//If you have to move to the parent you can use the following
console.log(p1.parentNode.parentNode);
//If you need the child elements with the text parts which include the non code
//texts also
console.log(p1.childNodes);
//If you only want the elements the use the followuing
console.log(p1.children);
*/

/* #14 Traversing through the DOM (Part 2) IN b/w Siblings*/

/*const bookList = document.querySelector('#box2');
// Moving to the next node
console.log('The next node is', bookList.nextSibling);
//Moving to the next element
console.log('The next element is', bookList.nextElementSibling);
//Moving to the previous node
console.log('The previous node is', bookList.previousSibling);
//Moving to the previous element
console.log('The previous element is', bookList.previousElementSibling);
// Appending some text in the paraghraph tag by the following method
 const HB2 = document.querySelector('.header');
 {HB2.parentNode.previousElementSibling.querySelector('h1').innerHTML +=
 ' Addedtext';} */

 /* #15 Working with Events */

 /* 1. We can react to an event by the following codeing. The element can be
 anything like text, button, search, etc.*/

 /*var h1 = document.querySelector('#box1 h1');
 h1.addEventListener('click', function(e){
   console.log(e.target); //This will show the element itself with its children.
   console.log(e); //This will show the properties of the event.
 } ); //You need to click the selected text to see the updates.*/

/* 2. Working with buttons when we select all buttons at once we have to work,
with each of the elements one by one for which we make it an array. If the
element selected is only one then we can apply the even listener directly.*/

/*const btns = document.querySelectorAll('.BookNames .delete');

Array.from(btns).forEach(function(btns){
  btns.addEventListener('click', function(e){
    var Li = e.target.parentElement.parentElement;
    console.log(Li);
    Li.parentElement.removeChild(Li);
    console.log('The Bookname', btns.parentElement.textcontent, 'has been deleted');
  });
});*/

/*3 Preventing an action of a link*/

/*var link = document.querySelector('#box2 p a');
link.addEventListener('click', function(e){
  e.preventDefault();
  console.log('The link', e.target.textContent, 'is pevented to work');
});
 // Make sure the link is working otherwise the value will show null. */

 /* #16 Event Bubbling With If Statement*/

 /*var list = document.querySelector('.BookNames ul');
 list.addEventListener('click',function(e){
   if(e.target.value == 'Delete'){
  var li = e.target.parentElement.parentElement;
   list.removeChild(li);
console.log('The bookname',e.target.parentElement.previousElementSibling.textContent,
     'is deleted');
 };
 });
*/

/* #17 Interacting with the forms*/

 // 1. We use the following to find the collection of forms
 //document.forms;
 // If you want to select one from it use the array form.

 // 2. Selecting the value for the text input in a form.


/*    // @1 Method by submitting value
 var addForm = document.forms['add-book'];
 addForm.addEventListener('submit', function(e){
   e.preventDefault();
   var value = addForm.querySelector('input[type="text"]').value;
   console.log('This is the text "', value, '" that we are going to add');
 }); // Submitting means entering the value using enter.

    // @2 Method by clicking the button for submitting.
var button = document.querySelector('form input[type="button"]')
 button.addEventListener('click', function(e){
   var value = addForm.querySelector('input[type="text"]').value;
   console.log('This is the text "', value, '" that we are going to add');
 }); // Click means we are using the button to enter the value.
*/

/* #18 Creating Elements without Styling*/

/*var addForm = document.forms['add-book'];
addForm.addEventListener('submit', function(e){
  e.preventDefault();
  var value = addForm.querySelector('input[type="text"]').value;
  console.log('This is the text "', value, '" that we are going to add');

  var li = document.createElement('li'); // Just creating new li to be added.
  var bookName = document.createElement('span'); //Similarly the Span is Created.
  var deleteBtn = document.createElement('span');
  var list = document.querySelector('.BookNames ul'); // Grabbing the ul for li input.

   bookName.textContent = value;
   deleteBtn.textContent = 'delete';

  li.appendChild(bookName);
  li.appendChild(deleteBtn);
  list.appendChild(li);

});*/

/* #19 Styling the Element and Classes.*/

/*var addForm = document.forms['add-book'];
addForm.addEventListener('submit', function(e){
  e.preventDefault();
  var value = addForm.querySelector('input[type="text"]').value;
  console.log('This is the text "', value, '" that we are going to add');

  var li = document.createElement('li'); // Just creating new li to be added.
  var bookName = document.createElement('span'); //Similarly the Span is Created.
  var deleteBtn = document.createElement('span');

  var list = document.querySelector('.BookNames ul'); // Grabbing the ul for li input.


   bookName.textContent = value;
   deleteBtn.value = 'Delete';

   bookName.classList = "name"; // This is how the classes are added to new elements.
   deleteBtn.classList = "delete";

   bookName.style.color = "Black"; // Just an example of Styling  within the javascript.
   bookName.style.marginLeft= "20px";

  li.appendChild(bookName);
  li.appendChild(deleteBtn);
  list.appendChild(li);

});*/

/* #20 Working with attributes */
/*    var bookname = document.querySelector('#box2 ul li:nth-child(3) .name');

// 1. Getting the attribute text of the element selected.
    console.log(bookname.getAttribute('class'));

// 2. Removing the attribute
    console.log(bookname.removeAttribute('class'));

// 3. Setting Up an attribute  // Setting up a type attribute. // It can change or add new attributes
console.log(bookname.setAttribute('class', 'neloy')); // Setting up a class
console.log(bookname.setAttribute('id', 'name')); //Setting up an id
console.log(bookname.setAttribute('type', 'button')); // Setting up a type attribute.

//4. Checking whether the element has an attribute or not.
console.log(bookname.hasAttribute('type'));*/

/* #21 Working with the Checkboxes and Change Events*/

/*1. For this practice remember we need a check input field in the HTML file*/

/*
const list = document.querySelector('#box2 ul');
const hideBox = document.querySelector('#hidebox');

hideBox.addEventListener('change', function(e){
    if(hideBox.checked) //This will check whether the box is checked or not. If check true value will be return otherwise false.
        {
            list.style.display="none"; // This will hide the ul.
        } else{
            list.style.display="initial"; // This will show up the ul again.
        }
}); */

/* #22 Custom Search Filter */

 /*   var list = document.querySelector('#box2 ul');
    const searchBar = document.querySelector('#search-books');

    searchBar.addEventListener('keyup', function(e){

        const term = e.target.value.toLowerCase();
        const books = list.querySelectorAll('li');

        for(i=0;i<books.length;i++){

        var title = books[i].querySelector('span.name').textContent;

            if(title.toLowerCase().indexOf(term) != -1){
            books[i].style.display = 'block';
        }else{
            books[i].style.display = 'none';
        }

        }

    });*/

/* #23 Creating Tabbed Contents */

/*var tabs = document.querySelector('.tabs');
 var panel = document.querySelectorAll('.panel');

tabs.addEventListener('click', function(e){
    if(e.target.tagName == 'li'){
        const targetPanel = document.querySelector(e.target.dataset.target);

        for(i=0;i<panel;i++){

            if(panel[i] == targetPanel){
                panel[i].classList.add('active');
            } else{
                panel[i].classList.remove('none');
            }
        }

    }
}); */

/* #24 DOM Content Loades Event */

 /*1. This event is only used when the javascript tag inn html file is
placed in the head section. All the javascript data in the JS file is entered
within this fuction*/

  document.addEventListener(DOMContentLoaded, function(){

    /* All the js content will be in b/w this if the js tag is placed in head
    section of the index or htmlk file*/
  });
